//
// Created by user on 10.11.21.
//

#ifndef MYFS_PROF_H
#define MYFS_PROF_H


#include <cstdint>
#include <ctime>

class prof {
public:
    char name[50];
    uint8_t id;
    uint16_t age;
    time_t creationTime;
    prof();
};


#endif //MYFS_PROF_H
