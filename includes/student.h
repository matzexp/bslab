//
// Created by user on 13.10.21.
//

#ifndef MYFS_STUDENT_H
#define MYFS_STUDENT_H


#include <ctime>
#include <vector>
#include <sys/stat.h>
#include <string>
#include <set>
#include <stdio.h>


using namespace std;

class student {

private:
    string name;
    time_t lastTimeSeenInTut;
    set<uint8_t>* tutsVisited;
    vector<bool>* goodQuestions;
    uint32_t counterForBadQuestions;
    
public:

    student(string _name);

    void visitedTut(uint8_t noOfTut);

    set<uint8_t>* getVisitedTuts();

    time_t getLastTimeSeen();

    string getName();

    void addQuestion(bool sinnvoll);

    bool wasQuestionGood(uint32_t index);

    float percentageGoodQuestion();


};


#endif //MYFS_STUDENT_H
