//
// Created by user on 13.10.21.
//

#include <stdint-gcc.h>
#include "student.h"

student::student(string _name) {
    name = _name;
    lastTimeSeenInTut = 0;
    tutsVisited = new set<uint8_t>();
    goodQuestions = new vector<bool>();
    counterForBadQuestions = 0;
}

void student::visitedTut(uint8_t noOfTut) {
    tutsVisited->insert(noOfTut);
    lastTimeSeenInTut  = time(0);
}

set<uint8_t>* student::getVisitedTuts() {
    set<uint8_t> *res = new set<uint8_t>();
    for (auto visited: *tutsVisited) {
        res->insert(visited);
    }
    return res;
}

time_t student::getLastTimeSeen() {
    return lastTimeSeenInTut;
}

string student::getName() {
    return name;
}


void student::addQuestion(bool sinnvoll) {
    goodQuestions->push_back(sinnvoll);
    if (!sinnvoll) {
        counterForBadQuestions++;
    }
}

bool student::wasQuestionGood(uint32_t index) {
    return goodQuestions->at(index);
}

float student::percentageGoodQuestion() {
    return float(counterForBadQuestions) / float(goodQuestions->size());
}